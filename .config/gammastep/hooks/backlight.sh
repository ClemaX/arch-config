#!/bin/bash

set -eu

CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/backlight"

TRANSITION_DURATION_US=10000000

CONTROLLER_KEYBOARD= # 'tpacpi::kbd_backlight'

LEVEL_MAX=100 
LEVEL_KEYBOARD_MAX=2

brightness.store()
{
	brillo -O
}

brightness.restore()
{
	brillo -u "$TRANSITION_DURATION_US" -I
}

brightness.set() # level 
{
	local level="$1"
	local controllers

	mapfile -t controllers < <(brillo -L)

	for controller in "${controllers[@]}"
	do
		brillo -s "$controller" \
			-u "$TRANSITION_DURATION_US" \
			-S "$level" &
	done

	if [ -n "$CONTROLLER_KEYBOARD" ]
	then
		awk > "/sys/class/leds/$CONTROLLER_KEYBOARD/brightness" '
				BEGIN {
					printf "%.0f\n", '"$LEVEL_KEYBOARD_MAX"' * '"$((100 - level))"' / '"$LEVEL_MAX"'
				}
'
	fi

	wait
}

brightness.load() # state
{
		local state="$1"

		local config_filepath="$CONFIG_DIR/$state"

		if [ -f "$config_filepath" ]
		then
				local level

				< "$config_filepath" read -r level

				if [[ "$level" =~ ^[[:digit:]]+$ ]]
				then
					brightness.set "$level"
				else
					echo "$config: level must be a number" >&2
					return 1
				fi
		fi
}

on_period_changed() # prev curr
{
	local prev="$1"
	local curr="$2"

	#notify-send "Gammastep" "$prev -> $curr"

	if [ "$prev" = none ]
	then
		brightness.store
	fi

	if [ "$curr" = none ]
	then
		brightness.restore
	else
		brightness.load "$curr"
	fi
}

event_type="$1"; shift

case "$event_type" in
    period-changed)
		on_period_changed "$@"
esac
