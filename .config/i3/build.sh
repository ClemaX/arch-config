#!/usr/bin/env bash

PARENT_DIR=$(dirname "$0")

CONFIG_DIR="$PARENT_DIR/config.d"
CONFIG_DEST="$PARENT_DIR/config"

exec 6>&1
exec >"${CONFIG_DEST}"

config_cat()
{
	dir_name="${1##*/}"
	dir_name="${dir_name##*-}"
	echo "### ${dir_name%.d}:"
	for file in "${1}"/**; do
		if [[ "${file}" == *.d ]]; then
			config_cat "${file}"
		else
			config_name="${file##*/}"
			config_name="${config_name##*-}"
			echo "### ${config_name} ###"
			cat "${file}"
			echo
		fi
	done
}

config_cat "${CONFIG_DIR}"

exec 1>&6 6>&-
