#!/bin/sh

file_save="$HOME/.cache/volume"

# Defaults
volume=0
cursor="│"
segment="─"
width=12
timeout=20
use_colors=0

usage() {
    printf "Usage:  $0 --bar [-c <cursor>] [-s <segment>] [-w <width>]\n\
[--colors <accent> <foreground> <background> [-t <timeout>]\n\
$0 [--up <amount>] [--down <amount>]\n"
    exit 1
}

url_base="http://moode.local"
url_get="$url_base/engine-mpd.php"
url_post="$url_base/command/moode.php?cmd=updvolume"

TEMP=$(getopt -o c:s:w: --long bar,colors:,up:,down: -n "$0" -- "$@")

if [ $? != 0 ]; then
    usage
fi

eval set -- "$TEMP"

read_volume() {
	[ -f "$file_save" ] || get_volume > /dev/null

    local volume=$(< "$file_save")

    [ "$volume" -ge 0 ] || volume=0 && echo "$volume" > "$file_save"

    [ $volume -gt 100 ] && volume=100 && echo "$volume" > "$file_save"

	echo "$volume"
}

get_volume() {
    local volume=$(curl -s $url_get | jq -r '.volume')

    echo "$volume" > "$file_save"

	echo "$volume"
}

post_volume() {
	local volume="${1:-0}"

    [ "$volume" -ge 0 ] || volume=0

    [ $volume -gt 100 ] && volume=100

    echo "$volume" > "$file_save"

    curl -s "$url_post" -X POST -d "volknob=$volume"
}

bar() {
	local factor=$(expr "100" "/" "$width")
	local volume
	local nFill nEmpty
	local fill empty

    while true :
    do
		volume=$(read_volume)

        nFill=$(expr "$volume" "/" "$factor")
        nEmpty=$(expr "$width" "-" "$nFill")

        fill=""
        empty=""

        if [ "$nFill" -gt 0 ]; then
            fill=$(printf -- "${segment}%.0s" $(seq 1 $nFill))
        fi

        if [ "$nEmpty" -gt 0 ]; then
            empty=$(printf -- "${segment}%.0s" $(seq 1 $nEmpty))
        fi

        if [ "$use_colors" -gt 0 ]; then
            echo "%{F$accent}$fill%{F$foreground}$cursor%{F$background}$empty%{F-}"
        else
            echo "$fill$cursor$empty"
        fi

        inotifywait -qe "MODIFY" "$file_save" -t "$timeout" > /dev/null
    done
}

up() {
    post_volume "$(expr "$(get_volume)" "+" "$1")"
    exit 0
}

down() {
    post_volume "$(expr "$(get_volume)" "+" "$1")"
    exit 0;
}

# Handle arguments
while true; do
    case "$1" in
        --bar)
            while true; do
                case "$2" in
                    "")	bar;;
                    -c)	cursor="$3"; shift 2;;
                    -s)	segment="$3"; shift 2;;
                    -w)	width="$3"; shift 2;;
                    -t)	timeout="$3"; shift 2;;
                    --colors)
                       	use_colors=1; accent="$3"
                       	foreground="$5"; background="$6"
                       	shift 5;;
                    --)	bar;;
                    *)	usage;;
                esac
            done
            ;;
        --up)	[ "$2" -lt 0 ] && usage || up "$2";;
        --down)	[ "$2" -lt 0 ] && usage || down "$2";;
        --)		shift; break;;
        *)		usage;;
    esac
done
