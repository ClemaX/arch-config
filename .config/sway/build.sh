#!/usr/bin/env bash

set -eu

PARENT_DIR=$(dirname "$0")

SYSTEM_CONFIG_DIR=/etc/sway/config.d
CONFIG_DIR=config.d

CONFIG_DEST=config

VARIANTS=(fx)

config() # dir_name
{
	local dir_name="$1"

	if [[ "$dir_name" != /* ]]
	then
		dir_name="${1#*/}"
	fi

	echo "include $dir_name/*"

	for file in "${1}"/**; do
		if [[ "${file}" == *.d ]]; then
			config "$dir_name/${file}"
		fi
	done
}

pushd "$PARENT_DIR"
	echo -e "# Generated file, do not edit manually!\n" > "$CONFIG_DEST"

	if [ -d "$SYSTEM_CONFIG_DIR" ]
	then
		config "$SYSTEM_CONFIG_DIR" >> "$CONFIG_DEST"
		echo >> "$CONFIG_DEST"
	fi

	config "$CONFIG_DIR" >> "$CONFIG_DEST"

	for variant in "${VARIANTS[@]}"
	do
		if [ -d "$variant.$CONFIG_DIR" ]
		then
			echo -e "# Generated file, do not edit manually!\n" > "$variant.$CONFIG_DEST"

			echo -e "include $CONFIG_DEST\n" >> "$variant.$CONFIG_DEST"

			config "$variant.$CONFIG_DIR" >> "$variant.$CONFIG_DEST"
		fi
	done
popd
