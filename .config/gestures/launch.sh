#!/usr/bin/env sh
echo "Killing Libinput-Gestures..."
libinput-gestures-setup stop
echo "Launching Libinput-Gestures..."
libinput-gestures -c $HOME/.config/gestures/config &
echo "Launched Libinput-Gestures!"
