#!/usr/bin/env sh

compositor=picom

if pgrep -x "$compositor" >/dev/null
then
    echo "Killing $compositor..."
    killall -q "$compositor"
else
	# shellcheck source=./launch.sh
	. "$HOME/.config/$compositor/launch.sh"
fi
