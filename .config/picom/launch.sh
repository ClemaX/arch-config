#!/bin/sh

compositor=picom

if pgrep -x "$compositor" >/dev/null
then
    echo "Killing $compositor..."
    killall -q "$compositor"
fi

echo "Launching $compositor..."
"$compositor" -b --config "$HOME/.config/$compositor/config"
