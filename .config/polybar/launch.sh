#!/usr/bin/env sh

BARS="top bottom"

BARS_EXTERNAL= #"HDMI1:external"

MONITORS=$(xrandr --listactivemonitors | tail -n +2 | cut -d ' ' --fields 6)

echo "Building config..."
~/.config/polybar/build.sh &

for monitor in $MONITORS
do
	for bar in $BARS_EXTERNAL
	do
		bar_monitor=${bar%%:*}
		bar_name=${bar#*:}

		if [ "$monitor" = "$bar_monitor" ]
		then
			BARS="$BARS $bar_name"
		fi
	done
done

RUNNING=$(pgrep --exact polybar --count)
TOTAL=$(echo "$BARS" | wc -w)

wait $!

if [ "$RUNNING" -eq "$TOTAL" ]
then
    echo "Restarting Polybars..."
	polybar-msg cmd restart
else
	if [ "$RUNNING" -gt 0 ]
	then
		echo "Killing Polybars..."
		killall polybar
	fi

	for bar_name in $BARS
	do
		polybar "$bar_name" &
	done
fi

echo "Launched Polybars!"
