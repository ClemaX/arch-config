#!/usr/bin/env sh

[ "$#" -lt 2 ] && echo "Usage: $0 BAR MSG" >&2 && exit 1

BAR="$1"; shift
MSG="$@"

if [ -w "/tmp/polybar_$BAR.ipc" ]
then
	echo "$MSG" > /tmp/polybar_$BAR.ipc
else
	echo "$BAR: Bar not found" >&2
	exit 2
fi
