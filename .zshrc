# Start desktop environment if local tty
if [[ -z $WAYLAND_DISPLAY ]] && [ "$XDG_VTNR" -eq 1 ]; then
	exec sway --unsupported-gpu
elif [[ $TERM = linux ]] && [ -r /opt/tty/dracula-tty.sh ]; then
	source /opt/tty/dracula-tty.sh
	DRACULA_ARROW_OVERRIDE='->'
fi

# Execute rehash on SIGUSR1
trap 'rehash' USR1

# Path to your oh-my-zsh installation.
if [ -d "$HOME/.oh-my-zsh" ]
then
	ZSH="$HOME/.oh-my-zsh"
else
	ZSH=/usr/share/oh-my-zsh
fi

export ZSH

ZSH_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/oh-my-zsh"

[ -d "$ZSH_CACHE_DIR" ] || mkdir -p "$ZSH_CACHE_DIR"

export GSETTINGS_SCHEMA_DIR=/usr/share/glib-2.0/schemas/

# Disable Dotnet Telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT="true"

export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim

ZSH_THEME=dracula
DRACULA_DISPLAY_CONTEXT=1

ENABLE_CORRECTION=false

DISABLE_UNTRACKED_FILES_DIRTY=true

unsetopt share_history

plugins=(
	git
	safe-paste
)

source "$ZSH/oh-my-zsh.sh"

[ -n "$DRACULA_ARROW_OVERRIDE" ] && DRACULA_ARROW_ICON="$DRACULA_ARROW_OVERRIDE"

# Up arrow history search
if [[ "${terminfo[kcuu1]}" != "" ]]; then
	autoload -U up-line-or-beginning-search
	autoload -U down-line-or-beginning-search
	zle -N up-line-or-beginning-search
	zle -N down-line-or-beginning-search
	bindkey "^[[A" up-line-or-beginning-search # Up
	bindkey "^[[B" down-line-or-beginning-search # Down
fi

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Initialize nvm
alias nvm-init='source /usr/share/nvm/init-nvm.sh'

# Initialize conda
alias conda-init='source /opt/miniconda3/etc/profile.d/conda.sh'

# i3 cheatsheet
alias i3-cheatsheet='egrep ^\\s\*bind ~/.config/i3/config \
| cut -d '\'' '\'' -f 2- | sed '\''s/ /\t/'\'' \
| column -ts $'\''\t'\'' | less'

# IDE
alias code='code-oss'

# config repo
CONFIG_DIR="$HOME/.config.git"
CONFIG_WORKTREE="$HOME"

alias config="/usr/bin/git --git-dir='$CONFIG_DIR' --work-tree='$CONFIG_WORKTREE'"
alias config-code="GIT_DIR='$CONFIG_DIR' GIT_WORK_TREE='$CONFIG_WORKTREE' code '$CONFIG_WORKTREE'"

# greeter
fortune -c | cowsay -f "$(ls /usr/share/cows | shuf -n1)"

# Android
export ANDROID_HOME=/opt/android-sdk

# perl5
export PERL_HOME="$HOME/.perl5"

export PERL5LIB="$PERL_HOME/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
export PERL_LOCAL_LIB_ROOT="${PERL_HOME}${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
export PERL_MB_OPT="--install_base \"$PERL_HOME\""
export PERL_MM_OPT="INSTALL_BASE=$PERL_HOME"

# go
export GOPATH="$HOME/.go"

# PATH
export PATH="$PERL_HOME/bin${PATH:+:${PATH}}:/opt/exploitdb";

# Autocompletion

autoload -Uz compinit
compinit

#source <(kubectl completion zsh) || :

if [ -d ~/.local/share/bash-completion/completions ]
then
	for file in ~/.local/share/bash-completion/completions/*
	do
		source "$file"
	done
fi

# Command not found
[ -f /etc/bash.command-not-found ] && source /etc/bash.command-not-found

# Synthax highlighted cat
alias ccat='pygmentize -g'

function chproj() # [PROJECT]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift
	cd "$(command chproj "$@")"
}

function mkproj() # [DIRECTORY]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift
	mkdir -p "$1" && chproj "$1"
}

function ghidra() # [PROJECT]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	if [ $# != 0 ]
	then
		local proj=$(realpath "$1"); shift

		[ -f proj ] || proj="$proj"/*.gpr
		set -- "$1" "$@"
	fi

	command ghidra "$@"
}

function mkscript() # [SCRIPTS]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	touch "$@" && chmod +x "$@"
}

function mkbash() # [SCRIPTS]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	for script in "$@"
	do
		! [ -e "$script" ] && cp ~/.config/mkscript/bash.sh "$script" && mkscript "$script"
		"${EDITOR:-vim}" "$script"
	done
}

function mkpython() # [SCRIPTS]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	for script in "$@"
	do
		! [ -e "$script" ] && cp ~/.config/mkscript/python.py "$script" && mkscript "$script"
	done
}

function mkruby() # [SCRIPTS]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	for script in "$@"
	do
		! [ -e "$script" ] && cp ~/.config/mkscript/ruby.rb "$script" && mkscript "$script"
	done
}

function mkphp() # [SCRIPTS]
{
	! [ $# = 0 ] && [ "$1" = "zle_bracketed_paste" ] && shift

	for script in "$@"
	do
		! [ -e "$script" ] && cp ~/.config/mkscript/php.php "$script" && mkscript "$script"
	done
}

[ "$PWD" != "$HOME" ] || chproj

# Default to intel assembler syntax
alias objdump='objdump -M intel'

# Default to wide readelf output
alias readelf='readelf -W'

# Use 4 as default tab width
tabs -4

# Default to firefox-developer-edition
alias firefox='firefox-developer-edition'

# Default to ncat
alias netcat='ncat'
alias nc='ncat'

# JWT decoder
alias jwtdecode='jq -R '\''split(".") | .[0],.[1] | @base64d | fromjson'\'

# SSH install terminfo-support
ssh-copy-terminfo() # [arguments]...
{
	infocmp | ssh $@ 'tic -x -'
}

# Java style
export JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on'
export _JAVA_OPTIONS="$JAVA_OPTIONS"
export JAVA_FONTS=/usr/share/fonts/TTF

eval "$(dircolors ~/.config/dircolors)"
